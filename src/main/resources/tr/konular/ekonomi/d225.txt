
2013-02-28T23:30:0Z
ekonomi
Hegel'in felsefesinin bu bilimin kendi tarihindeki birincil önemi bu tarihi evrensel usun açınımı olarak görüp birleştirerek onda bugüne dek başarılanların bir özetini vermesinde, başka bir deyişle, insanın kendi varoluşu üzerine şimdiye dek geliştirdiği kuramsal bilginin bir dizgesi ni sunmasında yatar. Bu değerlendirme felsefe tarihinin kendisinin edimsel tarihin öz bilinci olduğunu kabul eder. Bu düzeye dek Hegel'in felsefesi insanın, doğasının ve tarihinin Usun terimlerinde bir çözümlemesidir.(Kitabın İçinden) YAZAR: Georg Wilhelm Friedrich Hegel YAYINEVİ: İdea Yayınevi ISBN: 9753970889-70015 BOYUT: 13.0x20.0 SAYFA SAYISI: 402 BASIM YILI: 2004 CİLT DURUMU: Ciltli KAĞIT TÜRÜ: 1. Hamur DİL: Türkçe 3 11 2 0
