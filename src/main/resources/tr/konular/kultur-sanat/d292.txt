
2011-12-02T11:08:00Z
kultur-sanat
Bakırköy Belediyesi tarafından 3 Aralık Dünya Özürlüler Günü'nde düzenlenen Onur Akın Konseri yarın saat 14.00'de Bakırköy Belediyesi Atatürk Spor ve Yaşam Köyü Spor Salonu'nda yapılacak. Konseri ,Bakırköy Belediyesi'nin geçtiğimiz yaz Fethiye'de düzenlediği engelliler yaz kampına katılan yaklaşık 2000 özürlü ve ailesinin yanı sıra Bakırköylüler de ücretsiz izleyebilecek. "Kamp Ailesi Buluşuyor" etkinliği çerçevesinde ayrıca Atatürk Spor ve Yaşam Köyü İşçi Korosu ve Görme Engelliler Müzik Topluluğu konserleri de gerçekleşecek. YER:Bakırköy Belediyesi Atatürk Spor ve Yaşam Köyü Spor Salonu TARİH:3 Aralık 2011-Cumartesi SAAT:14.00 3 85 22 0
