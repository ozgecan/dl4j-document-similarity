
2011-03-12T12:05:00Z
kultur-sanat
Viyana'da sanat yaşamını sürdüren kemancı ve besteci Serkan Gürkan tarafından 2010 yılında kurulan Color Quartet, 14 Mart 2011 Pazartesi günü İzmir Sanat'ta müzikseverlerle buluşacak. Çelloda Yishu JIANG, Viyoloda Joachim BRANDL ve ikinci Kemanda Gabriele POITINGER'in yer aldığı grubun üyeleri, Avusturya dışındaki ilk konserlerini de İzmir Sanat'ta verecekler. Saat 20:00'de başlayacak konserin biletleri, İzmir Sanat'tan ya da www.izmir.bel.tr/kultursanat/anasayfa adreslerinden temin edilebilecek. 6 4 35 0
