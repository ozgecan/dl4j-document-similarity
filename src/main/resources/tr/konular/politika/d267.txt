
2012-07-01T15:30:0Z
politika
Başbakan Recep Tayyip Erdoğan partisinin genel kuruluna katılmak ve bir takım açılışlarda bulunmak için Kayseri'ye geldi. Kayseri Havalimanı'nda Başbakan Recep Tayyip Erdoğan'ı Enerji ve Tabi Kaynaklar Bakanı Taner Yıldız, TBMM Başkanvekili Sadık Yakut, Kayseri Valisi Mevlüt Bilici, Ak Parti Milletvekilleri Ahmet Öksüzkaya, Yaşar Karayel, Pelin Gündeş Bakır ve İsmail Tamer, Kayseri Emniyet Müdürü Cuma Ali Aydın, Kayseri Büyükşehir Belediye Başkanı Mehmet Özhaseki, Kayseri Garnizon Komutanı Tümgeneral Ali Demiral ve partililer karşıladı. Başbakan Erdoğan havalimanına inmeden önce Balyoz adlı köpek güvenlik araması yaptı. - KAYSERİ 4 94 16 0
