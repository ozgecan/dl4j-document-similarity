package com.deeplearning.word.similarity;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

public class WordSimilarity {

    public SentenceIterator iter;
    public TokenizerFactory token;
    public Word2Vec vector;

    public Word2Vec makeVector(String pathToRead) throws FileNotFoundException {
//         ClassPathResource resource = new ClassPathResource("raw_sentences.txt");
//        File file = resource.getFile();
        File file = new File(pathToRead);
        iter = new BasicLineIterator(file);
        token = new DefaultTokenizerFactory();
        token.setTokenPreProcessor(new CommonPreprocessor());

        vector = new Word2Vec.Builder()
                .layerSize(100)
                .windowSize(5)
                .iterate(iter)
                .tokenizerFactory(token)
                .build();
        vector.fit();

        return vector;
    }

    public double simOf2Words(String word1, String word2, Word2Vec vec) {
        double sim = vec.similarity(word1, word2);
        return sim;

    }

    public Collection<String> simOfMultWords(String word, int numOfWords, Word2Vec vec) {
        Collection<String> liste = vec.wordsNearest(word, numOfWords);
        return liste;
    }

}
