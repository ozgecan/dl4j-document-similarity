package com.deeplearning.paragraph.sim;

import java.io.File;
import java.io.FileNotFoundException;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.InMemoryLookupCache;
import org.deeplearning4j.text.documentiterator.LabelsSource;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

public class ParagraphSim {
// paragraflara lable atıp benzerliklerini labellar üzerinden karşılaştırıyor

    SentenceIterator iter;
    TokenizerFactory tokenizer;
    ParagraphVectors vec;

    public void paragraphSim(String pathToRead, String pathToWrite, int lineToCompare1, int lineToCompare2) throws FileNotFoundException {

//      ClassPathResource resource = new ClassPathResource("raw_sentences.txt");
//        File file = resource.getFile();
        File file = new File(pathToRead);
        iter = new BasicLineIterator(file);
        tokenizer = new DefaultTokenizerFactory();
        tokenizer.setTokenPreProcessor(new CommonPreprocessor());
        InMemoryLookupCache cache = new InMemoryLookupCache();
        LabelsSource source = new LabelsSource("satir_");

        vec = new ParagraphVectors.Builder()
                .minWordFrequency(5)
                .batchSize(300)
                .iterations(3)
                .epochs(5)
                .layerSize(2)
                //.learningRate(0.025)
                .labelsSource(source)
                .windowSize(5)
                .iterate(iter)
                .trainWordVectors(false)
                .vocabCache(cache)
                .tokenizerFactory(tokenizer)
                .sampling(0)
                .build();

        vec.fit();

        String a = "satir_" + lineToCompare1;
        String b = "satir_" + lineToCompare2;
        System.out.println("Labellar: " + source.getLabels() + "");
        double similarity1 = vec.similarity(a, b);
        System.out.println(a + " ile " + b + " arasındaki benzerlik: " + similarity1 + "");
        // Collection<String> lst = vec.nearestLabels(a, 2);
        // System.out.println("En yakın labeller : "+lst);
        String pathString = pathToWrite + "/paragraph_vector.txt";
        WordVectorSerializer.writeWordVectors(vec, pathString);

    }

}
