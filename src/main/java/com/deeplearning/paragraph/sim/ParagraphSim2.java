package com.deeplearning.paragraph.sim;

import java.io.File;
import java.io.FileNotFoundException;
import org.canova.api.util.ClassPathResource;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.InMemoryLookupCache;
import org.deeplearning4j.text.documentiterator.LabelsSource;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;

public class ParagraphSim2 {

    public static void main(String[] args) throws Exception {
        ParagraphSim2 p = new ParagraphSim2();
      p.testParagraphVectorsVocabBuilding1();
      // p.testParagraphVectorModelling1();
    }

    public void testParagraphVectorsVocabBuilding1() throws Exception {
        //ClassPathResource resource = new ClassPathResource("raw_sentences.txt");
      //  File file = resource.getFile();
      File file = new File ("/Users/SAMURAY/Desktop/r.txt");
        SentenceIterator iterator = new BasicLineIterator(file);

        int numberOfLines = 0;

        while (iterator.hasNext()) {

            iterator.nextSentence();

            numberOfLines++;

        }

        iterator.reset();

        InMemoryLookupCache cache = new InMemoryLookupCache(false);
        
        
        TokenizerFactory tokenizer = new DefaultTokenizerFactory();
        tokenizer.setTokenPreProcessor(new CommonPreprocessor());
        
        ParagraphVectors vector = new ParagraphVectors.Builder()
                .minWordFrequency(5)
                .iterations(5)
                .layerSize(100)
                .epochs(1)
                .windowSize(5)
                .iterate(iterator)
                .vocabCache(cache)
                .tokenizerFactory(tokenizer)
                .build();
        
        vector.buildVocab();
        LabelsSource source = vector.getLabelsSource();
        System.out.println("Satır sayısı : "+ numberOfLines);
        
        //assertEquals(numberOfLines, source.getLabels().size());

       // assertEquals(97162, source.getLabels().size());
        
        //assertEquals(97406, cache.numWords());
        
      //  assertEquals(244, cache.numWords() - source.getLabels().size());

        
        
                
    }
    public void testParagraphVectorModelling1() throws FileNotFoundException{
    ClassPathResource resource = new ClassPathResource("raw_sentences.txt");

    File file = resource.getFile();
    SentenceIterator iterator = new BasicLineIterator(file);
    InMemoryLookupCache cache = new InMemoryLookupCache(false);

     TokenizerFactory tokenizer = new DefaultTokenizerFactory();
     tokenizer.setTokenPreProcessor(new CommonPreprocessor());
     
     
      LabelsSource source = new LabelsSource("DOC_");




        ParagraphVectors vec = new ParagraphVectors.Builder()

                .minWordFrequency(1)

                .iterations(3)

                .epochs(1)

                .layerSize(100)

                .learningRate(0.025)

                .labelsSource(source)

                .windowSize(5)

                .iterate(iterator)

                .trainWordVectors(false)

                .vocabCache(cache)

                .tokenizerFactory(tokenizer)

                .sampling(0)

                .build();




        vec.fit();
        
        
        
        // Kelimelerin tekrar sayısını alıyor.
        
        int cnt1 = cache.wordFrequency("day");

        int cnt2 = cache.wordFrequency("me");

        System.out.println("Day : "+cnt1);
        System.out.println("Me : "+cnt2);
        
         double similarityD = vec.similarity("day", "night");

        System.out.println("day/night similarity: " + similarityD);



//
//        if (similarityD > 0.0) {
//
//           System.out.println("Day: " + Arrays.toString(vec.getWordVectorMatrix("day").dup().data().asDouble()));
//
//           System.out.println("Night: " + Arrays.toString(vec.getWordVectorMatrix("night").dup().data().asDouble()));
//
//        }


        

        //double similarityW = vec.similarity("way", "work");

        //System.out.println("way/work similarity: " + similarityW);




        double similarityH = vec.similarity("house", "world");

        System.out.println("house/world similarity: " + similarityH);




        double similarityC = vec.similarity("case", "way");

        System.out.println("case/way similarity: " + similarityC);


//        double similarity1 = vec.similarity("DOC_9835", "DOC_12492");
//
//        log.info("9835/12492 similarity: " + similarity1);

//        assertTrue(similarity1 > 0.7d);




//        double similarity2 = vec.similarity("DOC_3720", "DOC_16392");
//
//        log.info("3720/16392 similarity: " + similarity2);

//        assertTrue(similarity2 > 0.7d);




//        double similarity3 = vec.similarity("DOC_6347", "DOC_3720");
//
//        log.info("6347/3720 similarity: " + similarity3);

//        assertTrue(similarity2 > 0.7d);




        // likelihood in this case should be significantly lower

        double similarityX = vec.similarity("DOC_3720", "DOC_9852");

        System.out.println("3720/9852 similarity: " + similarityX);

       // assertTrue(similarityX < 0.5d);





    
    }
    
}
