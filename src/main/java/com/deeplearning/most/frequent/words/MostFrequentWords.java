package com.deeplearning.most.frequent.words;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.canova.api.util.ClassPathResource;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.InMemoryLookupCache;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

public class MostFrequentWords {
// Bir belgede en çok tekrar eden kelimelerin bulunması için kullanıldı.
    public  List<String> cacheList = new ArrayList<>();
     public    List<Integer> numOfwords = new ArrayList<>();
   
public void getMostFrequent(String textPath) throws FileNotFoundException, IOException{

        File file = new File(textPath);
        SentenceIterator iter = new BasicLineIterator(file);
        TokenizerFactory token = new DefaultTokenizerFactory();
        token.setTokenPreProcessor(new CommonPreprocessor());

//        
//         InMemoryLookupCache cache = new InMemoryLookupCache();
//        WeightLookupTable<VocabWord> table = new InMemoryLookupTable.Builder<VocabWord>()
//                .vectorLength(100)
//                .useAdaGrad(false)
//                .cache(cache)
//                .lr(0.025f).build();
//        
        VocabCache cache = new InMemoryLookupCache();
        List<String> stopWords = new ArrayList<>();
        ClassPathResource stop_res = new ClassPathResource("tr_stopwords.txt");
        //BufferedReader read = new BufferedReader(new FileReader("/Users/SAMURAY/Desktop/tr_stopwords.txt"));
        File stop_resource = stop_res.getFile();
        BufferedReader read = new BufferedReader(new FileReader(stop_resource));
        String ara;
        while ((ara = read.readLine()) != null) {
            stopWords.add(ara);
        }
        System.out.println("Stop_Words : " + stopWords.size());
        System.out.println(stopWords);
        Word2Vec vec = new Word2Vec.Builder()
                .minWordFrequency(5)
                .vocabCache(cache)
                .stopWords(stopWords)
                .iterations(1)
                .layerSize(100)
                .seed(42)
                .windowSize(5)
                .iterate(iter)
                .tokenizerFactory(token)
                .build();
        vec.fit();

        for (int i = 0; i < cache.numWords(); i++) {
            cacheList.add(cache.wordAtIndex(i));
            numOfwords.add(cache.wordFrequency(cache.wordAtIndex(i)));
        }
        System.out.println(cache.numWords() + "");
        System.out.println(cacheList + "");
        System.out.println(numOfwords + "");




}
}
