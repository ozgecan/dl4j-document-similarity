package com.deeplearnin4j.model.saving;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MakeCorpus {
// Birden fazla txt dökümanının birleştirilip tek corpus yapılması için kullanıldı

    public void makeCorpus(String path, String pathToWrite) throws FileNotFoundException {
        // Corpus yapılacak dosya tek değil ise liste kullanılabilir

        Scanner s = null;
//        List<String> liste = new ArrayList<>();
//        liste.add("dunya");
//        liste.add("spor");
//        liste.add("ekonomi");
//        liste.add("kultur-sanat");
//        liste.add("politika");
        PrintWriter write = null;
//        for (int i = 0; i < liste.size(); i++) {

//            String elem = liste.get(i);
//            String path = "/Users/HOME/Desktop/tr/konular/" + elem;
//            String pathToWrite = "/Users/HOME/Desktop/corpus/" + liste.get(i);
        write = new PrintWriter(pathToWrite);
        File dir = new File(path);

        for (File file : dir.listFiles()) {
            s = new Scanner(file);
            if (file.getName().charAt(0) == '.') {
                continue;
            }

            while (s.hasNext()) {

                String line = s.nextLine().replaceAll("\\p{Punct}|\\d", "").replaceAll("'", "");
                write.print(line);
            }
        }

//        }
        s.close();
        write.close();

    }

}
