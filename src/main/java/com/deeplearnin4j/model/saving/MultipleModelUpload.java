package com.deeplearnin4j.model.saving;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.deeplearning4j.berkeley.Pair;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.LabelSeeker;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.MeansBuilder;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.text.documentiterator.FileLabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelledDocument;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.ui.standalone.ClassPathResource;
import org.nd4j.linalg.api.ndarray.INDArray;

/**
 *
 * @author ozgecan
 */
public class MultipleModelUpload {

    public TokenizerFactory t;

    public MultipleModelUpload() {
        t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new CommonPreprocessor());
    }

    public ParagraphVectors uploadModel(String modelPath) throws FileNotFoundException {
        ParagraphVectors vector = WordVectorSerializer.readParagraphVectorsFromText(modelPath);
        return vector;
    }

    public void similarity() throws FileNotFoundException {
        List<String> labelName = new ArrayList<>();
        labelName.add("dunya");
        labelName.add("ekonomi");
        labelName.add("kultur-sanat");
        labelName.add("politika");
        labelName.add("spor");
        for (int i = 0; i < labelName.size(); i++) {
            ParagraphVectors vector = WordVectorSerializer.readParagraphVectorsFromText("/Users/HOME/Desktop/modeller/" + labelName.get(i) + "_model.json");

            System.out.println(i);
            
            ClassPathResource unlabeledResource = new ClassPathResource("tr/belirsiz");
            FileLabelAwareIterator unlabeledIterator = new FileLabelAwareIterator.Builder()
                    .addSourceFolder(unlabeledResource.getFile())
                    .build();

            MeansBuilder meansBuilder = new MeansBuilder((InMemoryLookupTable<VocabWord>) vector.getLookupTable(), t);
            LabelSeeker seeker = new LabelSeeker(vector.getLabelsSource().getLabels(), (InMemoryLookupTable<VocabWord>) vector.getLookupTable());

            while (unlabeledIterator.hasNextDocument()) {
                LabelledDocument document = unlabeledIterator.nextDocument();

                INDArray documentAsCentroid = meansBuilder.documentAsVector(document);
                List<Pair<String, Double>> scores = seeker.getScores(documentAsCentroid);

                System.out.println("'" + document.getLabel() + "' kategorisi  için sonuçlar aşağıdadır. ");
                for (Pair<String, Double> score : scores) {
                    System.out.println("        " + score.getFirst() + ": " + score.getSecond());
                    if (score.getSecond() > 0) {
                        System.out.println("Bu döküman  için incelenecek  sınıflar :" + score.getFirst());
                    }
                }
            }
        }

    }

}
