package com.deeplearnin4j.model.saving;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.canova.api.util.ClassPathResource;
import org.deeplearning4j.berkeley.Pair;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.LabelSeeker;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.MeansBuilder;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.text.documentiterator.FileLabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelledDocument;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.ops.transforms.Transforms;

public class SeperatedModels {

    LabelAwareIterator iterator;

    TokenizerFactory tokenizerFactory;
    List<String> liste = new ArrayList<>();

    public SeperatedModels() throws FileNotFoundException {
        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());

        liste.add("dunya");
        liste.add("spor");
        liste.add("ekonomi");
        liste.add("kultur-sanat");
        liste.add("politika");
    }

    public void makeModels() throws FileNotFoundException, IOException {

        List<String> stopWords = new ArrayList<>();
        ClassPathResource stop_res = new ClassPathResource("stop-words-tr.txt");
        //BufferedReader read = new BufferedReader(new FileReader("/Users/SAMURAY/Desktop/tr_stopwords.txt"));
        File stop_resource = stop_res.getFile();
        BufferedReader read = new BufferedReader(new FileReader(stop_resource));
        String ara;
        while ((ara = read.readLine()) != null) {
            stopWords.add(ara);
        }
        System.out.println("Stopwords Boyutu :" + stopWords.size());
        String path = "/Users/HOME/Desktop/tr/Konular";
        File resource = new File(path);
        iterator = new FileLabelAwareIterator.Builder().addSourceFolder(resource).build();

        ParagraphVectors paragraphVectors = new ParagraphVectors.Builder()
                .learningRate(0.025)
                .minLearningRate(0.001)
                .batchSize(1000)
                .epochs(10)
                .stopWords(stopWords)
                .iterate(iterator)
                .trainWordVectors(true)
                .tokenizerFactory(tokenizerFactory)
                .build();

        paragraphVectors.fit();

        WordVectorSerializer.writeWordVectors(paragraphVectors, "/Users/HOME/Desktop/dunya_model.json");
    }

    public void checkUnlabeledData() throws FileNotFoundException {

        File file_ = new File("/Users/HOME/Desktop/dunya_model.json");
        ClassPathResource unClassifiedResource = new ClassPathResource("tr/belirsiz");

        FileLabelAwareIterator unClassifiedIterator = new FileLabelAwareIterator.Builder()
                .addSourceFolder(unClassifiedResource.getFile())
                .build();
        ParagraphVectors paragraphVectors_ = WordVectorSerializer.readParagraphVectorsFromText(file_);
        MeansBuilder meansBuilder = new MeansBuilder(
                (InMemoryLookupTable<VocabWord>) paragraphVectors_.getLookupTable(),
                tokenizerFactory);
        List<String> labels = paragraphVectors_.getLabelsSource().getLabels();
        System.out.println("Labellar: " + labels + " ");

        LabelSeeker seeker = new LabelSeeker(paragraphVectors_.getLabelsSource().getLabels(),
                (InMemoryLookupTable<VocabWord>) paragraphVectors_.getLookupTable());

        while (unClassifiedIterator.hasNextDocument()) {
            LabelledDocument document = unClassifiedIterator.nextDocument();
            INDArray documentAsCentroid = meansBuilder.documentAsVector(document);
            List<Pair<String, Double>> scores = seeker.getScores(documentAsCentroid);

            System.out.println("Document '" + document.getLabel() + "' falls into the following categories: ");

            for (Pair<String, Double> score : scores) {
                System.out.println("        " + score.getFirst() + ": " + score.getSecond());
            }
        }

    }

    public List<Pair<String, Double>> getSim(List<String> liste, INDArray documentAsCentroid) {
        List<Pair<String, Double>> result = new ArrayList<>();
        for (int i = 0; i < liste.size(); i++) {
            String path = "/Users/HOME/Desktop/m/" + liste.get(i) + "_model.json";
            INDArray model = (INDArray) new WordVectorSerializer().readParagraphVectorsFromText(path);

            double sim = Transforms.cosineSim(model, documentAsCentroid);
            result.add(new Pair<String, Double>(liste.get(i), sim));

        }
        return result;
    }

}
