package com.deeplearnin4j.model.saving;

/**
 *
 * @author YNS
 */
import java.io.FileNotFoundException;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;


public class ModelSaving {

    public void save_model() throws FileNotFoundException {

        String filePath = "src/main/resources/text_sim/LOTR.txt";
        SentenceIterator iterator = new BasicLineIterator(filePath);
        TokenizerFactory token = new DefaultTokenizerFactory();
        token.setTokenPreProcessor(new CommonPreprocessor());
        System.out.println("Vektör builder'a giriyor.");
        Word2Vec vector = new Word2Vec.Builder()
                .iterations(1)
                .epochs(2)
                .layerSize(100)
                .windowSize(5)
                .learningRate(0.05)
                .iterate(iterator)
                .tokenizerFactory(token)
                .build();

        vector.fit();
        System.out.println("Model oluşturuldu.");

        WordVectorSerializer.writeFullModel(vector, "/Users/HOME/Desktop/model.json");
        
        System.out.println("Model kaydedildi.");

    }

    public Word2Vec get_model() throws FileNotFoundException {
        Word2Vec vector = WordVectorSerializer.loadFullModel("/Users/HOME/Desktop/model.json");
        return vector;
    }

    
}
