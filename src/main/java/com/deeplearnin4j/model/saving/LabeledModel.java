/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deeplearnin4j.model.saving;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.canova.api.util.ClassPathResource;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.documentiterator.FileLabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelAwareIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

/**
 *
 * @author YNS
 */
public class LabeledModel {
//
    
    public ParagraphVectors paragraphVectors;
    public LabelAwareIterator iterator;
    public TokenizerFactory tokenizerFactory;
    public Word2Vec paragraphmodel;
    public FileLabelAwareIterator unClassifiedIterator;
    public List<String> liste = new ArrayList<String>();

    public LabeledModel() throws FileNotFoundException {
        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
        ClassPathResource resource = new ClassPathResource("tr/konular");
        unClassifiedIterator = new FileLabelAwareIterator.Builder().addSourceFolder(resource.getFile()).build();
        liste = unClassifiedIterator.getLabelsSource().getLabels();
        System.out.println(liste + "");
//        paragraphmodel = WordVectorSerializer.loadFullModel("/Users/HOME/Desktop/paragraph_model.json");
//        WeightLookupTable weightLookupTable = paragraphmodel.lookupTable();
//
//
//        Iterator<INDArray> vectors = weightLookupTable.vectors();
    }

   public void makeParagraphVectorsAndModel() throws Exception {
        paragraphVectors = new ParagraphVectors.Builder()
                .learningRate(0.025)
                .minLearningRate(0.001)
                .batchSize(1000)
                .epochs(1)
                .iterate(unClassifiedIterator)
                .labels(liste)
                .labelsSource(unClassifiedIterator.getLabelsSource())
                .trainWordVectors(true)
                .tokenizerFactory(tokenizerFactory)
                .build();

        paragraphVectors.fit();
        int i = unClassifiedIterator.getLabelsSource().size();
        System.out.println(i);
        int i2 = paragraphVectors.getLabelsSource().size();
        System.out.println(i2);
//        boolean b=unClassifiedIterator.hasNextDocument();
//        System.out.println(b);
//        while(iterator.hasNextDocument()){
//        System.out.println(iterator.getLabelsSource().nextLabel());
//        }
        WordVectorSerializer.writeFullModel(paragraphVectors, "/Users/HOME/Desktop/labeled_model.json");
    }

}
