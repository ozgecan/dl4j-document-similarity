package com.deeplearnin4j.model.saving;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.canova.api.util.ClassPathResource;
import org.deeplearning4j.berkeley.Pair;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.LabelSeeker;
import org.deeplearning4j.examples.nlp.paragraphvectors.tools.MeansBuilder;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.text.documentiterator.FileLabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelledDocument;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;

public class MakeModel {

    public LabelAwareIterator iterator;
    public TokenizerFactory tokenizerFactory;

    public MakeModel() {
        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());

    }

    public void MakeModels() throws FileNotFoundException, IOException {
        List<String> stopWords = new ArrayList<>();
        ClassPathResource stop_res = new ClassPathResource("stop-words-tr.txt");
        File stop_resource = stop_res.getFile();
        BufferedReader read = new BufferedReader(new FileReader(stop_resource));
        String ara;
        while ((ara = read.readLine()) != null) {
            stopWords.add(ara);
        }
        System.out.println("Stopwords Boyutu :" + stopWords.size());

        String path = "/Users/HOME/Desktop/Konular";
        File resource = new File(path);
        iterator = new FileLabelAwareIterator.Builder().addSourceFolder(resource).build();

        System.out.println("İterator oluşturuldu");

        ParagraphVectors paragraphVectors = new ParagraphVectors.Builder()
                .learningRate(0.025)
                .minLearningRate(0.001)
                .batchSize(1000)
                .epochs(10)
                .stopWords(stopWords)
                .iterate(iterator)
                .trainWordVectors(true)
                .tokenizerFactory(tokenizerFactory)
                .build();
        System.out.println("Build etti. ");
        paragraphVectors.fit();
        System.out.println("Fit etti. ");

        WordVectorSerializer.writeWordVectors(paragraphVectors, "/Users/HOME/Desktop/spor_model.json");
    }

    public void checkUnlabelled() throws FileNotFoundException {
        ClassPathResource resource = new ClassPathResource("tr/belirsiz");
        List<String> liste = new ArrayList<String>();
        liste.add("dunya");
        liste.add("spor");
        liste.add("ekonomi");
        liste.add("kultur-sanat");
        liste.add("politika");

        for (int i = 0; i < liste.size(); i++) {

            System.out.println((i + 1) + ". döngü.");
            String model = liste.get(i);
            File modelFile = new File("/Users/HOME/Desktop/modeller/" + model + "_model.json");
            System.out.println("");
            System.out.println(model + " modeline benzerliği ölçülüyor. . ");
            System.out.println("");

            FileLabelAwareIterator unClassifiedIterator = new FileLabelAwareIterator.Builder()
                    .addSourceFolder(resource.getFile())
                    .build();
            System.out.println(unClassifiedIterator.getLabelsSource().getLabels() + " labellar ");
            ParagraphVectors paragraphVectors = WordVectorSerializer.readParagraphVectorsFromText(modelFile);

            MeansBuilder meansBuilder = new MeansBuilder(
                    (InMemoryLookupTable<VocabWord>) paragraphVectors.getLookupTable(),
                    tokenizerFactory);
            LabelSeeker seeker = new LabelSeeker(paragraphVectors.getLabelsSource().getLabels(),
                    (InMemoryLookupTable<VocabWord>) paragraphVectors.getLookupTable());
            int counter = 0;
            System.out.println(unClassifiedIterator.getLabelsSource().getNumberOfLabelsUsed() + " labelsused sayısı ");

            while (unClassifiedIterator.hasNextDocument()) {
                LabelledDocument document = unClassifiedIterator.nextDocument();

                INDArray documentAsCentroid = meansBuilder.documentAsVector(document);

                List<Pair<String, Double>> scores = seeker.getScores(documentAsCentroid);

                System.out.println("Document '" + document.getLabel() + "' falls into the following categories: ");
                System.out.println("length :" + scores.size());
                for (Pair<String, Double> score : scores) {
                    System.out.println("        " + score.getFirst() + ": " + score.getSecond());
                }
                counter++;

            }
            System.out.println("Counter değeri : " + counter);
        }
    }

}
