/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deeplearning4j.model.saving_test;

import com.deeplearnin4j.model.saving.ModelSaving;
import java.util.Arrays;
import java.util.Collection;
import org.deeplearning4j.models.word2vec.Word2Vec;

/**
 *
 * @author YNS
 */
public class ModelSavingTest {
    public static void main(String[] args) throws Exception {
        ModelSaving m = new ModelSaving();
        m.save_model();

        Word2Vec vector = m.get_model();
        Collection<String> liste = vector.wordsNearest(Arrays.asList("gandalf", "frodo"), Arrays.asList("ring"), 10);
        System.out.println(liste);
    }

}
