/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deeplearning.model.visualizing_test;

import com.deeplearning.model.visualizing.ModelVisualizing;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.plot.BarnesHutTsne;
import org.nd4j.linalg.api.buffer.DataBuffer;
import org.nd4j.linalg.factory.Nd4j;

/**
 *
 * @author YNS
 */
public class ModelVisualizingTest {

    public static void main(String[] args) throws Exception {
        ModelVisualizing t = new ModelVisualizing();
        int iterations = 1000;
        Nd4j.dtype = DataBuffer.Type.DOUBLE;
        Nd4j.factory().setDType(DataBuffer.Type.DOUBLE);
        List<String> cacheList = new ArrayList<>();
        System.out.println("Load & Vectorize data....");
        File wordFile = new org.springframework.core.io.ClassPathResource("words.txt").getFile();

        WordVectors v = ModelVisualizing.loadTxtVectors(new FileInputStream(wordFile), false);

        VocabCache cache = v.vocab();

        InMemoryLookupTable weights = (InMemoryLookupTable) v.lookupTable();
        for (int i = 0; i < cache.numWords(); i++) {
            System.out.println("Cachelist: " + i);
            cacheList.add(cache.wordAtIndex(i));
        }
        System.out.println("Build model....");
        BarnesHutTsne tsne = new BarnesHutTsne.Builder()
                .setMaxIter(iterations)
                .normalize(false)
                .learningRate(500)
                .useAdaGrad(false)
                .usePca(false)
                .build();

        System.out.println("Store TSNE Coordinates for Plotting....");
        String outputFile = "/Users/HOME/Desktop/tsne-standard-coords.csv";

        (new File(outputFile)).getParentFile().mkdirs();
        tsne.plot(weights.getSyn0(), 2, cacheList, outputFile);

        System.out.println("Yazdırıldı..");
    }

}
